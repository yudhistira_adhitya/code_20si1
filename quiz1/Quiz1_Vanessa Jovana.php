<?php 

#Vanessa Jovana - 03081200016 - 20SI1

echo "<table style='border-color:black; border-style:solid; border-width:2px;'>";
echo "<tr style='background-color:#f00; color:white; text-align:center;'>";
	echo "<th style='width:100px;'>Faktorial</th>";
	echo "<th style='width:300px;'>Hasil</th>";
echo "</tr>";

for ($i=1; $i < 21; $i++) { 
	if ($i%2==0) { #genap
		$warna='#cce8fb';
	}
	else { #ganjil
		$warna='#fbf6cc';
	}
	echo "<tr style='color:black; background-color:$warna; text-align:center;'>";

	echo "<td>$i</td>";

	$hasilfaktorial = 1;
	for ($j=1; $j<=$i ; $j++) { 
		$hasilfaktorial*=$j; #hasilfaktorial = j * hasilfaktorial
	}

	echo "<td>$hasilfaktorial</td>";
	echo "</tr>";
}

echo "</table>";
 ?>