<?php

require "config.php";
$nama = $_POST['nama'];
$alamat = $_POST['alamat'];
$no_hp = $_POST['no_hp'];
$id = $_POST['id'];

$query = "UPDATE kontak SET nama = ?, alamat = ?, no_hp=? WHERE id=?";
$stmt = $conn->prepare($query);
$stmt->bind_param("sssd",$nama,$alamat,$no_hp,$id);
if($stmt->execute()){
    header("Location: kontak.php");
} else {
    echo "data gagal di edit" . $stmt->error;
}

?>