<?php
require "config.php";
$nama = $_POST['nama'];
$alamat = $_POST['alamat'];
$no_hp = $_POST['no_hp'];

$sql = "INSERT INTO kontak (nama,alamat,no_hp) VALUES (:nama,:alamat,:no_hp)";
$query = $conn->prepare($sql);
$hasil = $query->execute(array(
    'nama' => $nama,
    'alamat' => $alamat,
    'no_hp' => $no_hp
));

if ($hasil) {
    header('Location: kontak.php');
} else {
    echo "gagal tambah data";
};
