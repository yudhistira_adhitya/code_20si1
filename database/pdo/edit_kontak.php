<?php
require "config.php";

$id = $_GET['id'];

$sql = "SELECT * FROM kontak WHERE id=:id";
$query = $conn->prepare($sql);
$query->execute(array(
    'id' => $id
));
$row = $query->fetch();

?>
<!DOCTYPE html>
<html>
<head>
	<title>Edit Data Kontak</title>
</head>
<body>
	<h2>Edit Kontak</h2>
	<form action="edit_kontak_proses.php" method="post">
		Nama : <input type="text" name="nama" value="<?= $row['nama'] ?>"><br/>
        Alamat : <textarea name="alamat"><?= $row['alamat'] ?></textarea><br/>
		No. Telp : <input type="text" name="no_hp" value="<?= $row['no_hp'] ?>"><br/>
        <input type="hidden" name="id" value="<?= $row['id'] ?>">
		<input type="submit">
	</form>
</body>
</html>