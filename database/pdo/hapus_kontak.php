<?php
require "config.php";

$id = $_GET['id'];

$sql = "DELETE FROM kontak WHERE id=:id";
$query = $conn->prepare($sql);
$hasil = $query->execute(array(
    'id' => $id
));

if ($hasil) {
    header('Location: kontak.php');
} else {
    echo "gagal hapus data";
};
