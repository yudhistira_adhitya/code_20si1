<?php

require "config.php";
$nama = $_POST['nama'];
$alamat = $_POST['alamat'];
$no_hp = $_POST['no_hp'];
$id = $_POST['id'];

$sql = "UPDATE kontak SET nama = :nama, alamat = :alamat, no_hp=:no_hp WHERE id=:id";
$query = $conn->prepare($sql);
$hasil = $query->execute(array(
    'id' => $id,
    'nama' => $nama,
    'no_hp' => $no_hp,
    'alamat'=> $alamat
));

if ($hasil) {
    header('Location: kontak.php');
} else {
    echo "gagal edit data";
};
