<?php require('config.php');
    if(is_logged_in()){
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php require('head.php');?>
</head>
<body>
    <div class="container">
    <?php require('header.php');?>
       
        <section>
                <h1>Halaman Messages</h1>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
        </section>
    </div>
    
</body>
</html>
<?php  } else {
    header('Location: login.php');
}
